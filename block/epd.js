(() => {

'use strict';
goog.require('Blockly.Blocks');
goog.require('Mixly.Boards');

const { Boards } = Mixly;
const BOARDS_TYPE = Boards.getType();
const EPD_HUE = 180;
const SVG_PATH = "./libraries/ThirdParty/EPD/media/epd.svg";
const EPD_BRIEF = {
    "GxDEPG0150BN": "1.50寸, 黑/白",
    "GxGDEP015OC1": "1.54寸, 黑/白",
    "GxGDEH0154D67": "1.54寸, 黑/白, 200×200, SSD1681",
    "GxGDEW0154T8": "1.54寸, 黑/白, 152×152, [UC8151, IL0373]",
    "GxGDEW0154M09": "1.54寸, 黑/白, 200×200, JD79653A",
    "GxGDEW0154M10": "1.54寸, 黑/白, 152×152, UC8151D",
    "GxGDEW0154Z04": "1.54寸, 黑/白, 200×200",
    "GxGDEW0154Z17": "1.54寸, 黑/白/红, 152×152",
    "GxGDEH0154Z90": "1.54寸, 黑/白/红, 200×200, SSD1681",
    "GxGDEW0213I5F": "2.13寸, 黑/白, 104×212",
    "GxGDE0213B1": "2.13寸, 黑/白",
    "GxGDEH0213B72": "2.13寸, 黑/白",
    "GxGDEH0213B73": "2.13寸, 黑/白",
    "GxGDEM0213B74": "2.13寸, 黑/白, 128×250, SSD1680",
    "GxGDEW0213Z16": "2.13寸, 黑/白/红, UC8151D",
    "GxGDEH0213Z19": "2.13寸, 黑/白/红",
    "GxGDEW0213T5D": "2.13寸, 黑/白, 104×212, UC8151D",
    "GxDEPG0213BN": "2.13寸, 黑/白, 128×250, SSD1680",
    "GxGDEH029A1": "2.90寸, 黑/白",
    "GxGDEW029T5": "2.90寸, 黑/白, [UC8151, IL0373]",
    "GxGDEW029T5D": "2.90寸, 黑/白, UC8151D",
    "GxGDEM029T94": "2.90寸, 黑/白",
    "GxDEPG0290BS": "2.90寸, 黑/白",
    "GxGDEW029Z10": "2.90寸, 黑/白/红",
    "GxGDEH029Z13": "2.90寸, 黑/白/红, UC8151D",
    "GxGDEW026T0": "2.60寸, 黑/白",
    "GxDEPG0266BN": "2.66寸, 黑/白, 152×296, SSD1680",
    "GxGDEW027C44": "2.70寸, 黑/白/红",
    "GxGDEW027W3": "2.70寸, 黑/白",
    "GxGDEY027T91": "2.70寸, 黑/白",
    "GxGDEW0371W7": "3.70寸, 黑/白",
    "GxGDEW042T2": "4.20寸, 黑/白",
    "GxGDEW042Z15": "4.20寸, 黑/白/红",
    "GxGDEW0583T7": "5.83寸, 黑/白",
    "GxGDEW075T8": "7.50寸, 黑/白",
    "GxGDEW075T7": "7.50寸, 黑/白, 800×480",
    "GxGDEW075Z09": "7.50寸, 黑/白/红",
    "GxGDEW075Z08": "7.50寸, 黑/白/红, 800×480",
};

Blockly.Blocks.epd_init = {
    init: function () {
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_LEFT)
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("初始化墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "NAME")
            .appendField(new Blockly.FieldDropdown([
                ["GxDEPG0150BN", "GxDEPG0150BN"],
                ["GxGDEP015OC1", "GxGDEP015OC1"],
                ["GxGDEH0154D67", "GxGDEH0154D67"],
                ["GxGDEW0154T8", "GxGDEW0154T8"],
                ["GxGDEW0154M09", "GxGDEW0154M09"],
                ["GxGDEW0154M10", "GxGDEW0154M10"],
                ["GxGDEW0154Z04", "GxGDEW0154Z04"],
                ["GxGDEW0154Z17", "GxGDEW0154Z17"],
                ["GxGDEH0154Z90", "GxGDEH0154Z90"],
                ["GxGDEW0213I5F", "GxGDEW0213I5F"],
                ["GxGDE0213B1", "GxGDE0213B1"],
                ["GxGDEH0213B72", "GxGDEH0213B72"],
                ["GxGDEH0213B73", "GxGDEH0213B73"],
                ["GxGDEM0213B74", "GxGDEM0213B74"],
                ["GxGDEW0213Z16", "GxGDEW0213Z16"],
                ["GxGDEH0213Z19", "GxGDEH0213Z19"],
                ["GxGDEW0213T5D", "GxGDEW0213T5D"],
                ["GxDEPG0213BN", "GxDEPG0213BN"],
                ["GxGDEH029A1", "GxGDEH029A1"],
                ["GxGDEW029T5", "GxGDEW029T5"],
                ["GxGDEW029T5D", "GxGDEW029T5D"],
                ["GxGDEM029T94", "GxGDEM029T94"],
                ["GxDEPG0290BS", "GxDEPG0290BS"],
                ["GxGDEW029Z10", "GxGDEW029Z10"],
                ["GxGDEH029Z13", "GxGDEH029Z13"],
                ["GxGDEW026T0", "GxGDEW026T0"],
                ["GxDEPG0266BN", "GxDEPG0266BN"],
                ["GxGDEW027C44", "GxGDEW027C44"],
                ["GxGDEW027W3", "GxGDEW027W3"],
                ["GxGDEY027T91", "GxGDEY027T91"],
                ["GxGDEW0371W7", "GxGDEW0371W7"],
                ["GxGDEW042T2", "GxGDEW042T2"],
                ["GxGDEW042Z15", "GxGDEW042Z15"],
                ["GxGDEW0583T7", "GxGDEW0583T7"],
                ["GxGDEW075T8", "GxGDEW075T8"],
                ["GxGDEW075T7", "GxGDEW075T7"],
                ["GxGDEW075Z09", "GxGDEW075Z09"],
                ["GxGDEW075Z08", "GxGDEW075Z08"]
            ], function (value) {
                var block = this.getSourceBlock();
                block.setFieldValue(EPD_BRIEF[value], 'BRIEF');
                return value;
            }), "TYPE")
            .appendField(new Blockly.FieldLabel(EPD_BRIEF[this.getFieldValue('TYPE')]), "BRIEF");
        this.appendValueInput("RST")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("RST#");
        this.appendValueInput("DC")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("DC#");
        this.appendValueInput("CS")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("CS#");
        this.appendValueInput("BUSY")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("BUSY#");
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_draw_paged_to_window = {
    init: function () {
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_LEFT)
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "NAME")
            .appendField("绘制页面到屏幕");
        this.appendValueInput("X")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("左上角x");
        this.appendValueInput("Y")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("y");
        this.appendValueInput("WIDTH")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("宽度");
        this.appendValueInput("HEIGHT")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("高度");
        this.appendValueInput("CB")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("执行");
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};


//设置光标的位置，EPD将会从此位置开始，向后显示文本或数字
Blockly.Blocks.epd_set_cursor = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 设置光标位置为");
        this.appendValueInput("set_cursor_x")
            .setCheck(null)
            .appendField("X#");
        this.appendValueInput("set_cursor_y")
            .setCheck(null)
            .appendField("Y#");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("设置光标的位置，EPD将会从此位置开始，向后显示文本或数字");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_set_font = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 设置字体尺寸为");
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([["微小", "0"], ["小", "1"], ["中", "2"], ["大", "3"], ["超大", "4"]]), "font_size");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("EPD设置字体的大小");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_setTextSize = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 设置字体尺寸");
        this.appendValueInput("size")
            .setCheck(null);
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("EPD设置字体的大小");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_TextSize = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([["微小", "0"], ["小", "1"], ["中", "2"], ["大", "3"], ["超大", "4"]]), "size");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("EPD字体尺寸");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_set_font_color = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 设置字体为");
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([
                ["黑色", "GxEPD_BLACK"],
                ["白色", "GxEPD_WHITE"],
                ["红色", "GxEPD_RED"],
                ["深灰色", "GxEPD_DARKGREY"],
                ["浅灰色", "GxEPD_LIGHTGREY"]
            ]), "font_color");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("EPD设置字体的颜色");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_setTextColor = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 设置字体颜色");
        this.appendValueInput("color")
            .setCheck(null);
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("EPD设置字体的颜色");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_TextColor = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([
                ["黑色", "GxEPD_BLACK"],
                ["白色", "GxEPD_WHITE"],
                ["红色", "GxEPD_RED"],
                ["深灰色", "GxEPD_DARKGREY"],
                ["浅灰色", "GxEPD_LIGHTGREY"]
            ]), "color");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("EPD字体颜色");
        this.setHelpUrl("");
    }
};

//设置字体和字体背景颜色
Blockly.Blocks.epd_set_text_background_color = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 设置字体颜色");
        this.appendValueInput("set_text_color_data")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("字体");
        this.appendValueInput("set_text_color_background")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("背景");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_show_text = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(new Blockly.FieldDropdown([["显示文本", "print"], ["显示文本(自动换行)", "println"]]), "show_text_type");
        this.appendValueInput("show_text_auto_linefeed_data")
            .setCheck(null);
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_show_num = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(new Blockly.FieldDropdown([["显示数字", "print"], ["显示数字(自动换行)", "println"]]), "show_num_type");
        this.appendValueInput("show_num_auto_linefeed_data")
            .setCheck(null);
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([["二进制", "BIN"], ["八进制", "OCT"], ["十进制", "DEC"], ["十六进制", "HEX"]]), "show_num_auto_linefeed_type");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_set_rotation = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 旋转屏幕")
            .appendField(new Blockly.FieldDropdown([["0°", "0"], ["90°", "1"], ["180°", "2"], ["270°", "3"]]), "set_rotation_data");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_setRotation = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 屏幕旋转");
        this.appendValueInput("angle")
            .setCheck(null);
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("EPD旋转屏幕");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_Rotation = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([["0°", "0"], ["90°", "1"], ["180°", "2"], ["270°", "3"]]), "angle");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("角度值");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_clear_update_display = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(new Blockly.FieldDropdown([["清屏", "eraseDisplay"], ["刷新页面", "update"]]), "choose_type");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_draw_Color = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([
                ["黑色", "GxEPD_BLACK"],
                ["白色", "GxEPD_WHITE"],
                ["红色", "GxEPD_RED"],
                ["深灰色", "GxEPD_DARKGREY"],
                ["浅灰色", "GxEPD_LIGHTGREY"]
            ]), "color");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("EPD绘图颜色");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_draw_pixel = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 画点");
        this.appendValueInput("draw_pixel_x")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("x");
        this.appendValueInput("draw_pixel_y")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("y");
        this.appendValueInput("color")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("颜色");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("EPD画点");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_draw_line = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 画线");
        this.appendValueInput("draw_line_start_x")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("起点x");
        this.appendValueInput("draw_line_start_y")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("y");
        this.appendValueInput("draw_line_end_x")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("终点x");
        this.appendValueInput("draw_line_end_y")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("y");
        this.appendValueInput("color")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("颜色");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("EPD画线");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_draw_line1 = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(new Blockly.FieldDropdown([["画水平线", "drawFastHLine"], ["画垂直线", "drawFastVLine"]]), "draw_line1_type");
        this.appendValueInput("draw_line_start_x")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("起点x");
        this.appendValueInput("draw_line_start_y")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("y");
        this.appendValueInput("draw_line_length")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("长度");
        this.appendValueInput("color")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("颜色");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_draw_rect = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 画矩形")
            .appendField(new Blockly.FieldDropdown([["空心", "draw"], ["实心", "fill"]]), "draw_rect_type_1");
        this.appendValueInput("draw_rect_x")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("左上角x");
        this.appendValueInput("draw_rect_y")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("y");
        this.appendValueInput("draw_rect_width")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("宽度");
        this.appendValueInput("draw_rect_height")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("高度");
        this.appendValueInput("color")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("颜色");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("EPD绘制矩形");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_draw_RoundRect = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 画圆角矩形")
            .appendField(new Blockly.FieldDropdown([["空心", "draw"], ["实心", "fill"]]), "draw_rect_type_1");
        this.appendValueInput("draw_rect_x")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("左上角x");
        this.appendValueInput("draw_rect_y")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("y");
        this.appendValueInput("draw_rect_width")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("宽度");
        this.appendValueInput("draw_rect_height")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("高度");
        this.appendValueInput("draw_rect_radius")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("半径");
        this.appendValueInput("color")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("颜色");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("EPD绘制圆角矩形");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_draw_circle = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 画圆")
            .appendField(new Blockly.FieldDropdown([["空心", "draw"], ["实心", "fill"]]), "draw_circle_type");
        this.appendValueInput("draw_circle_x")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("圆心x");
        this.appendValueInput("draw_circle_y")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("y");
        this.appendValueInput("draw_circle_radius")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("半径");
        this.appendValueInput("color")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("颜色");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("EPD绘制圆");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_draw_CircleHelper = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 画圆弧");
        this.appendValueInput("draw_circle_x")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("圆心x#");
        this.appendValueInput("draw_circle_y")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("y#");
        this.appendValueInput("draw_circle_radius")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("半径");
        this.appendValueInput("draw_circle_cornername")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("弧段");
        this.appendValueInput("color")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("颜色");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("EPD绘制圆弧");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_draw_CircleHelper_data = {
    init: function () {
        this.appendDummyInput()
            .appendField("左上")
            .appendField(new Blockly.FieldCheckbox("true"), "LEFT_UP")
            .appendField(new Blockly.FieldCheckbox("true"), "RIGHT_UP")
            .appendField("右上");
        this.appendDummyInput()
            .appendField("左下")
            .appendField(new Blockly.FieldCheckbox("true"), "LEFT_DOWN")
            .appendField(new Blockly.FieldCheckbox("true"), "RIGHT_DOWN")
            .appendField("右下");
        this.setOutput(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_fill_CircleHelper = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 画实心圆弧");
        this.appendValueInput("draw_circle_x")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("圆心x#");
        this.appendValueInput("draw_circle_y")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("y#");
        this.appendValueInput("draw_circle_radius")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("半径");
        this.appendValueInput("draw_circle_cornername")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("弧段");
        this.appendValueInput("draw_circle_delta")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("长度");
        this.appendValueInput("color")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("颜色");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_fill_CircleHelper_data = {
    init: function () {
        this.appendDummyInput()
            .appendField("左上&左下")
            .appendField(new Blockly.FieldCheckbox("true"), "LEFT");
        this.appendDummyInput()
            .appendField("右上&右下")
            .appendField(new Blockly.FieldCheckbox("true"), "RIGHT");
        this.setOutput(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_draw_ellipse = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 画椭圆")
            .appendField(new Blockly.FieldDropdown([["空心", "false"], ["实心", "true"]]), "draw_ellipse_type");
        this.appendValueInput("draw_ellipse_x")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("圆心x");
        this.appendValueInput("draw_ellipse_y")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("y");
        this.appendValueInput("draw_ellipse_x_radius")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("x方向半径");
        this.appendValueInput("draw_ellipse_y_radius")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("y方向半径");
        this.appendValueInput("draw_ellipse_cornername")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("弧段");
        this.appendValueInput("color")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("颜色");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_draw_triangle = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 画三角形")
            .appendField(new Blockly.FieldDropdown([["空心", "draw"], ["实心", "fill"]]), "draw_triangle_type");
        this.appendValueInput("draw_triangle_x1")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("x1");
        this.appendValueInput("draw_triangle_y1")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("y1");
        this.appendValueInput("draw_triangle_x2")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("x2");
        this.appendValueInput("draw_triangle_y2")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("y2");
        this.appendValueInput("draw_triangle_x3")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("x3");
        this.appendValueInput("draw_triangle_y3")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("y3");
        this.appendValueInput("color")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("颜色");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("EPD绘制三角形");
        this.setHelpUrl("");
    }
};

Blockly.Blocks.epd_draw_bitmap = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage(SVG_PATH, 20, 20, "*"))
            .appendField("墨水屏")
            .appendField(new Blockly.FieldTextInput("display"), "name")
            .appendField(" 画位图");
        this.appendValueInput("draw_bitmap_x")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("左上角x");
        this.appendValueInput("draw_bitmap_y")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("y");
        this.appendValueInput("draw_bitmap_data")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("位图名称");
        this.appendValueInput("draw_bitmap_width")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("宽度");
        this.appendValueInput("draw_bitmap_height")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("高度");
        this.appendValueInput("color")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("颜色");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(EPD_HUE);
        this.setTooltip("EPD画位图");
        this.setHelpUrl("");
    }
};


})();
