(() => {

'use strict'; 
goog.require('Blockly.Arduino');
goog.require('Mixly.Boards');

const { Boards } = Mixly;
const BOARDS_TYPE = Boards.getType();

Blockly.Arduino.epd_init = function () {
    var FIELD_NAME = this.getFieldValue("NAME");
    var FIELD_TYPE = this.getFieldValue("TYPE");
    var VALUE_INPUT_RST = Blockly.Arduino.valueToCode(this, "RST", Blockly.Arduino.ORDER_ATOMIC);
    var VALUE_INPUT_DC = Blockly.Arduino.valueToCode(this, "DC", Blockly.Arduino.ORDER_ATOMIC);
    var VALUE_INPUT_CS = Blockly.Arduino.valueToCode(this, "CS", Blockly.Arduino.ORDER_ATOMIC);
    var VALUE_INPUT_BUSY = Blockly.Arduino.valueToCode(this, "BUSY", Blockly.Arduino.ORDER_ATOMIC);

    Blockly.Arduino.definitions_['include_GxEPD'] = '#include <GxEPD.h>';
    Blockly.Arduino.definitions_[`include_${FIELD_TYPE}/${FIELD_TYPE}`] = `#include <${FIELD_TYPE}/${FIELD_TYPE}.h>`;
    Blockly.Arduino.definitions_['include_GxIO/GxIO_SPI/GxIO_SPI'] = '#include <GxIO/GxIO_SPI/GxIO_SPI.h>';
    Blockly.Arduino.definitions_['include_GxIO/GxIO'] = '#include <GxIO/GxIO.h>';
    Blockly.Arduino.definitions_[`var_declare_${FIELD_NAME}_io`] = `GxIO_Class ${FIELD_NAME}_io(SPI, ${VALUE_INPUT_CS}, ${VALUE_INPUT_DC}, ${VALUE_INPUT_RST});`;
    Blockly.Arduino.definitions_[`var_declare_${FIELD_NAME}`] = `GxEPD_Class ${FIELD_NAME}(${FIELD_NAME}_io, ${VALUE_INPUT_RST}, ${VALUE_INPUT_BUSY});`;
    Blockly.Arduino.setups_['setup_' + FIELD_NAME] = FIELD_NAME + '.init();';
    return "";
};

Blockly.Arduino.epd_draw_paged_to_window = function () {
    var FIELD_NAME = this.getFieldValue("NAME");
    var VALUE_INPUT_X = Blockly.Arduino.valueToCode(this, "X", Blockly.Arduino.ORDER_ATOMIC);
    var VALUE_INPUT_Y = Blockly.Arduino.valueToCode(this, "Y", Blockly.Arduino.ORDER_ATOMIC);
    var VALUE_INPUT_WIDTH = Blockly.Arduino.valueToCode(this, "WIDTH", Blockly.Arduino.ORDER_ATOMIC);
    var VALUE_INPUT_HEIGHT = Blockly.Arduino.valueToCode(this, "HEIGHT", Blockly.Arduino.ORDER_ATOMIC);
    var VALUE_INPUT_CB = Blockly.Arduino.valueToCode(this, "CB", Blockly.Arduino.ORDER_ATOMIC);
    return `${FIELD_NAME}.drawPagedToWindow(${VALUE_INPUT_CB}, ${VALUE_INPUT_X}, ${VALUE_INPUT_Y}, ${VALUE_INPUT_WIDTH}, ${VALUE_INPUT_HEIGHT});\n`;
};

//设置光标的位置，OLED将会从此位置开始，向后显示文本或数字
Blockly.Arduino.epd_set_cursor = function () {
    var text_name = this.getFieldValue('name');
    var value_set_cursor_x = Blockly.Arduino.valueToCode(this, 'set_cursor_x', Blockly.Arduino.ORDER_ATOMIC);
    var value_set_cursor_y = Blockly.Arduino.valueToCode(this, 'set_cursor_y', Blockly.Arduino.ORDER_ATOMIC);
    var code = text_name + '.setCursor(' + value_set_cursor_x + ', ' + value_set_cursor_y + ');\n';
    return code;
};

Blockly.Arduino.epd_set_font = function () {
    var text_name = this.getFieldValue('name');
    var dropdown_font_size = this.getFieldValue('font_size');
    var code = text_name + '.setTextSize(' + dropdown_font_size + ');\n';
    return code;
};

Blockly.Arduino.epd_setTextSize = function () {
    var text_name = this.getFieldValue('name');
    var value_size = Blockly.Arduino.valueToCode(this, 'size', Blockly.Arduino.ORDER_ATOMIC) || '1';
    var code = text_name + '.setTextSize(' + value_size + ');\n';
    return code;
};

Blockly.Arduino.epd_TextSize = function () {
    var dropdown_size = this.getFieldValue('size');
    var code = dropdown_size;
    return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.epd_set_font_color = function () {
    var text_name = this.getFieldValue('name');
    var dropdown_font_color = this.getFieldValue('font_color');
    var code = text_name + '.setTextColor(' + dropdown_font_color + ');\n';
    return code;
};

Blockly.Arduino.epd_setTextColor = function () {
    var text_name = this.getFieldValue('name');
    var value_color = Blockly.Arduino.valueToCode(this, 'color', Blockly.Arduino.ORDER_ATOMIC) || '1';
    var code = text_name + '.setTextColor(' + value_color + ');\n';
    return code;
};

Blockly.Arduino.epd_set_text_background_color = function () {
    var text_name = this.getFieldValue('name');
    var value_set_text_color_data = Blockly.Arduino.valueToCode(this, 'set_text_color_data', Blockly.Arduino.ORDER_ATOMIC) || '1';
    var value_set_text_color_background = Blockly.Arduino.valueToCode(this, 'set_text_color_background', Blockly.Arduino.ORDER_ATOMIC) || '0';
    var code = text_name + '.setTextColor(' + value_set_text_color_data + ', ' + value_set_text_color_background + ');\n';
    return code;
};

Blockly.Arduino.epd_TextColor = function () {
    var dropdown_color = this.getFieldValue('color');
    var code = dropdown_color;
    return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.epd_show_text = function () {
    var text_name = this.getFieldValue('name');
    var dropdown_show_text_type = this.getFieldValue('show_text_type');
    var value_show_text_auto_linefeed_data = Blockly.Arduino.valueToCode(this, 'show_text_auto_linefeed_data', Blockly.Arduino.ORDER_ATOMIC);
    var code = text_name + '.' + dropdown_show_text_type + '(' + value_show_text_auto_linefeed_data + ');\n';
    return code;
};

Blockly.Arduino.epd_show_num = function () {
    var text_name = this.getFieldValue('name');
    var dropdown_show_num_type = this.getFieldValue('show_num_type');
    var value_show_num_auto_linefeed_data = Blockly.Arduino.valueToCode(this, 'show_num_auto_linefeed_data', Blockly.Arduino.ORDER_ATOMIC);
    var dropdown_show_num_auto_linefeed_type = this.getFieldValue('show_num_auto_linefeed_type');
    var code = text_name + '.' + dropdown_show_num_type + '(' + value_show_num_auto_linefeed_data + ', ' + dropdown_show_num_auto_linefeed_type + ');\n';
    return code;
};

Blockly.Arduino.epd_set_rotation = function () {
    var text_name = this.getFieldValue('name');
    var dropdown_set_rotation_data = this.getFieldValue('set_rotation_data');
    var code = '' + text_name + '.setRotation(' + dropdown_set_rotation_data + ');\n';
    return code;
};

Blockly.Arduino.epd_setRotation = function () {
    var text_name = this.getFieldValue('name');
    var value_angle = Blockly.Arduino.valueToCode(this, 'angle', Blockly.Arduino.ORDER_ATOMIC) || '0';
    var code = text_name + '.setRotation(' + value_angle + ');\n';
    return code;
};

Blockly.Arduino.epd_Rotation = function () {
    var dropdown_angle = this.getFieldValue('angle');
    var code = dropdown_angle;
    return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.epd_clear_update_display = function () {
    var text_name = this.getFieldValue('name');
    var dropdown_choose_type = this.getFieldValue('choose_type');
    var code = text_name + '.' + dropdown_choose_type + '();\n';
    return code;
};

Blockly.Arduino.epd_draw_Color = function () {
    var dropdown_color = this.getFieldValue('color');
    var code = dropdown_color;
    return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.epd_draw_pixel = function () {
    var text_name = this.getFieldValue('name');
    var value_draw_pixel_x = Blockly.Arduino.valueToCode(this, 'draw_pixel_x', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_pixel_y = Blockly.Arduino.valueToCode(this, 'draw_pixel_y', Blockly.Arduino.ORDER_ATOMIC);
    var value_color = Blockly.Arduino.valueToCode(this, 'color', Blockly.Arduino.ORDER_ATOMIC) || '1';
    var code = text_name + '.drawPixel(' + value_draw_pixel_x + ', ' + value_draw_pixel_y + ', ' + value_color + ');\n';
    return code;
};

Blockly.Arduino.epd_draw_line = function () {
    var text_name = this.getFieldValue('name');
    var value_draw_line_start_x = Blockly.Arduino.valueToCode(this, 'draw_line_start_x', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_line_start_y = Blockly.Arduino.valueToCode(this, 'draw_line_start_y', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_line_end_x = Blockly.Arduino.valueToCode(this, 'draw_line_end_x', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_line_end_y = Blockly.Arduino.valueToCode(this, 'draw_line_end_y', Blockly.Arduino.ORDER_ATOMIC);
    var value_color = Blockly.Arduino.valueToCode(this, 'color', Blockly.Arduino.ORDER_ATOMIC) || '1';
    var code = text_name + '.drawLine(' + value_draw_line_start_x + ', ' + value_draw_line_start_y + ', ' + value_draw_line_end_x + ', ' + value_draw_line_end_y + ', ' + value_color + ');\n';
    return code;
};

Blockly.Arduino.epd_draw_line1 = function () {
    var text_name = this.getFieldValue('name');
    var dropdown_draw_line1_type = this.getFieldValue('draw_line1_type');
    var value_draw_line_start_x = Blockly.Arduino.valueToCode(this, 'draw_line_start_x', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_line_start_y = Blockly.Arduino.valueToCode(this, 'draw_line_start_y', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_line_length = Blockly.Arduino.valueToCode(this, 'draw_line_length', Blockly.Arduino.ORDER_ATOMIC);
    var value_color = Blockly.Arduino.valueToCode(this, 'color', Blockly.Arduino.ORDER_ATOMIC) || '1';
    var code = '' + text_name + '.' + dropdown_draw_line1_type + '(' + value_draw_line_start_x + ', ' + value_draw_line_start_y + ', ' + value_draw_line_length + ', ' + value_color + ');\n';
    return code;
};

Blockly.Arduino.epd_draw_rect = function () {
    var text_name = this.getFieldValue('name');
    var dropdown_draw_rect_type_1 = this.getFieldValue('draw_rect_type_1');
    var value_draw_rect_x = Blockly.Arduino.valueToCode(this, 'draw_rect_x', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_rect_y = Blockly.Arduino.valueToCode(this, 'draw_rect_y', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_rect_width = Blockly.Arduino.valueToCode(this, 'draw_rect_width', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_rect_height = Blockly.Arduino.valueToCode(this, 'draw_rect_height', Blockly.Arduino.ORDER_ATOMIC);
    var value_color = Blockly.Arduino.valueToCode(this, 'color', Blockly.Arduino.ORDER_ATOMIC) || '1';
    var code = text_name + '.' + dropdown_draw_rect_type_1 + 'Rect(' + value_draw_rect_x + ', ' + value_draw_rect_y + ', ' + value_draw_rect_width + ', ' + value_draw_rect_height + ', ' + value_color + ');\n';
    return code;
};

Blockly.Arduino.epd_draw_RoundRect = function () {
    var text_name = this.getFieldValue('name');
    var dropdown_draw_rect_type_1 = this.getFieldValue('draw_rect_type_1');
    var value_draw_rect_x = Blockly.Arduino.valueToCode(this, 'draw_rect_x', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_rect_y = Blockly.Arduino.valueToCode(this, 'draw_rect_y', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_rect_width = Blockly.Arduino.valueToCode(this, 'draw_rect_width', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_rect_height = Blockly.Arduino.valueToCode(this, 'draw_rect_height', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_rect_radius = Blockly.Arduino.valueToCode(this, 'draw_rect_radius', Blockly.Arduino.ORDER_ATOMIC);
    var value_color = Blockly.Arduino.valueToCode(this, 'color', Blockly.Arduino.ORDER_ATOMIC) || '1';
    var code = text_name + '.' + dropdown_draw_rect_type_1 + 'RoundRect(' + value_draw_rect_x + ', ' + value_draw_rect_y + ', ' + value_draw_rect_width + ', ' + value_draw_rect_height + ', ' + value_draw_rect_radius + ', ' + value_color + ');\n';
    return code;
};

Blockly.Arduino.epd_draw_circle = function () {
    var text_name = this.getFieldValue('name');
    var dropdown_draw_circle_type = this.getFieldValue('draw_circle_type');
    var value_draw_circle_x = Blockly.Arduino.valueToCode(this, 'draw_circle_x', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_circle_y = Blockly.Arduino.valueToCode(this, 'draw_circle_y', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_circle_radius = Blockly.Arduino.valueToCode(this, 'draw_circle_radius', Blockly.Arduino.ORDER_ATOMIC);
    var value_color = Blockly.Arduino.valueToCode(this, 'color', Blockly.Arduino.ORDER_ATOMIC) || '1';
    var code = text_name + '.' + dropdown_draw_circle_type + 'Circle(' + value_draw_circle_x + ', ' + value_draw_circle_y + ', ' + value_draw_circle_radius + ', ' + value_color + ');\n';
    return code;
};

Blockly.Arduino.epd_draw_CircleHelper = function () {
    var text_name = this.getFieldValue('name');
    var value_draw_circle_x = Blockly.Arduino.valueToCode(this, 'draw_circle_x', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_circle_y = Blockly.Arduino.valueToCode(this, 'draw_circle_y', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_circle_radius = Blockly.Arduino.valueToCode(this, 'draw_circle_radius', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_circle_cornername = Blockly.Arduino.valueToCode(this, 'draw_circle_cornername', Blockly.Arduino.ORDER_ATOMIC) || '0x00';
    var value_color = Blockly.Arduino.valueToCode(this, 'color', Blockly.Arduino.ORDER_ATOMIC) || '1';
    var code = text_name + '.drawCircleHelper(' + value_draw_circle_x + ', ' + value_draw_circle_y + ', ' + value_draw_circle_radius + ', ' + value_draw_circle_cornername + ', ' + value_color + ');\n';
    return code;
};

Blockly.Arduino.epd_draw_CircleHelper_data = function () {
    var checkbox_LEFT_UP = this.getFieldValue('LEFT_UP') == 'TRUE';
    var checkbox_RIGHT_UP = this.getFieldValue('RIGHT_UP') == 'TRUE';
    var checkbox_LEFT_DOWN = this.getFieldValue('LEFT_DOWN') == 'TRUE';
    var checkbox_RIGHT_DOWN = this.getFieldValue('RIGHT_DOWN') == 'TRUE';
    var code = 'B0000' + (checkbox_LEFT_DOWN ? '1' : '0') + (checkbox_RIGHT_DOWN ? '1' : '0') + (checkbox_RIGHT_UP ? '1' : '0') + (checkbox_LEFT_UP ? '1' : '0');
    return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.epd_fill_CircleHelper = function () {
    var text_name = this.getFieldValue('name');
    var value_draw_circle_x = Blockly.Arduino.valueToCode(this, 'draw_circle_x', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_circle_y = Blockly.Arduino.valueToCode(this, 'draw_circle_y', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_circle_radius = Blockly.Arduino.valueToCode(this, 'draw_circle_radius', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_circle_cornername = Blockly.Arduino.valueToCode(this, 'draw_circle_cornername', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_circle_delta = Blockly.Arduino.valueToCode(this, 'draw_circle_delta', Blockly.Arduino.ORDER_ATOMIC);
    var value_color = Blockly.Arduino.valueToCode(this, 'color', Blockly.Arduino.ORDER_ATOMIC) || '1';
    var code = text_name + '.fillCircleHelper(' + value_draw_circle_x + ', ' + value_draw_circle_y + ', ' + value_draw_circle_radius + ', ' + value_draw_circle_cornername + ', ' + value_draw_circle_delta + ', ' + value_color + ');\n';
    return code;
};

Blockly.Arduino.epd_fill_CircleHelper_data = function () {
    var checkbox_LEFT = this.getFieldValue('LEFT') == 'TRUE';
    var checkbox_RIGHT = this.getFieldValue('RIGHT') == 'TRUE';
    var code = 'B000000' + (checkbox_RIGHT ? '1' : '0') + (checkbox_LEFT ? '1' : '0');
    return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.epd_draw_ellipse = function () {
    var text_name = this.getFieldValue('name');
    var dropdown_draw_ellipse_type = this.getFieldValue('draw_ellipse_type');
    var value_draw_ellipse_x = Blockly.Arduino.valueToCode(this, 'draw_ellipse_x', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_ellipse_y = Blockly.Arduino.valueToCode(this, 'draw_ellipse_y', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_ellipse_x_radius = Blockly.Arduino.valueToCode(this, 'draw_ellipse_x_radius', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_ellipse_y_radius = Blockly.Arduino.valueToCode(this, 'draw_ellipse_y_radius', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_ellipse_cornername = Blockly.Arduino.valueToCode(this, 'draw_ellipse_cornername', Blockly.Arduino.ORDER_ATOMIC);
    var value_color = Blockly.Arduino.valueToCode(this, 'color', Blockly.Arduino.ORDER_ATOMIC) || '1';
    Blockly.Arduino.definitions_['function_draw_ellipse_section'] = 'void draw_ellipse_section(Adafruit_GFX *screen, int16_t x, int16_t y, int16_t x0, int16_t y0, uint8_t option, boolean fill, uint16_t color){\n'
        + '  /* upper left */\n'
        + '  if (option & 0x01){\n'
        + '    if(fill)\n'
        + '      screen->drawFastVLine(x0-x, y0-y, y+1, color);\n'
        + '    else\n'
        + '      screen->drawPixel(x0 - x, y0 - y, color);\n'
        + '  }\n'
        + '\n'
        + '  /* upper right */\n'
        + '  if (option & 0x02){\n'
        + '    if(fill)\n'
        + '      screen->drawFastVLine(x0+x, y0-y, y+1, color);\n'
        + '    else\n'
        + '      screen->drawPixel(x0 + x, y0 - y, color);\n'
        + '  }\n'
        + '\n'
        + '  /* lower right */\n'
        + '  if (option & 0x04){\n'
        + '    if(fill)\n'
        + '      screen->drawFastVLine(x0+x, y0, y+1, color);\n'
        + '    else\n'
        + '      screen->drawPixel(x0 + x, y0 + y, color);\n'
        + '  }\n'
        + '\n'
        + '  /* lower left */\n'
        + '  if (option & 0x08){\n'
        + '    if(fill)\n'
        + '      screen->drawFastVLine(x0-x, y0, y+1, color);\n'
        + '    else\n'
        + '      screen->drawPixel(x0 - x, y0 + y, color);\n'
        + '  }\n'
        + '}\n';
    Blockly.Arduino.definitions_['function_draw_ellipse'] = 'void draw_ellipse(Adafruit_GFX *screen, int16_t x0, int16_t y0, int16_t rx, int16_t ry, uint8_t option, boolean fill, uint16_t color){\n'
        + '  uint16_t x, y;\n'
        + '  int32_t xchg, ychg;\n'
        + '  int32_t err;\n'
        + '  int32_t rxrx2;\n'
        + '  int32_t ryry2;\n'
        + '  int32_t stopx, stopy;\n'
        + '\n'
        + '  rxrx2 = rx;\n'
        + '  rxrx2 *= rx;\n'
        + '  rxrx2 *= 2;\n'
        + '\n'
        + '  ryry2 = ry;\n'
        + '  ryry2 *= ry;\n'
        + '  ryry2 *= 2;\n'
        + '\n'
        + '  x = rx;\n'
        + '  y = 0;\n'
        + '\n'
        + '  xchg = 1;\n'
        + '  xchg -= rx;\n'
        + '  xchg -= rx;\n'
        + '  xchg *= ry;\n'
        + '  xchg *= ry;\n'
        + '\n'
        + '  ychg = rx;\n'
        + '  ychg *= rx;\n'
        + '\n'
        + '  err = 0;\n'
        + '\n'
        + '  stopx = ryry2;\n'
        + '  stopx *= rx;\n'
        + '  stopy = 0;\n'
        + '\n'
        + '  while( stopx >= stopy ){\n'
        + '    draw_ellipse_section(screen, x, y, x0, y0, option, fill, color);\n'
        + '    y++;\n'
        + '    stopy += rxrx2;\n'
        + '    err += ychg;\n'
        + '    ychg += rxrx2;\n'
        + '    if ( 2*err+xchg > 0 ){\n'
        + '      x--;\n'
        + '      stopx -= ryry2;\n'
        + '      err += xchg;\n'
        + '      xchg += ryry2;\n'
        + '    }\n'
        + '  }\n'
        + '\n'
        + '  x = 0;\n'
        + '  y = ry;\n'
        + '\n'
        + '  xchg = ry;\n'
        + '  xchg *= ry;\n'
        + '\n'
        + '  ychg = 1;\n'
        + '  ychg -= ry;\n'
        + '  ychg -= ry;\n'
        + '  ychg *= rx;\n'
        + '  ychg *= rx;\n'
        + '\n'
        + '  err = 0;\n'
        + '\n'
        + '  stopx = 0;\n'
        + '\n\n'
        + '  stopy = rxrx2;\n'
        + '  stopy *= ry;\n'
        + '\n'
        + '  while( stopx <= stopy ){\n'
        + '    draw_ellipse_section(screen, x, y, x0, y0, option, fill, color);\n'
        + '    x++;\n'
        + '    stopx += ryry2;\n'
        + '    err += xchg;\n'
        + '    xchg += ryry2;\n'
        + '    if ( 2*err+ychg > 0 ){\n'
        + '      y--;\n'
        + '      stopy -= rxrx2;\n'
        + '      err += ychg;\n'
        + '      ychg += rxrx2;\n'
        + '    }\n'
        + '  }\n'
        + '}\n';
    var code = 'draw_ellipse(&' + text_name + ', ' + value_draw_ellipse_x + ', ' + value_draw_ellipse_y + ', ' + value_draw_ellipse_x_radius + ', ' + value_draw_ellipse_y_radius + ', ' + value_draw_ellipse_cornername + ', ' + dropdown_draw_ellipse_type + ', ' + value_color + ');\n';
    return code;
};

Blockly.Arduino.epd_draw_triangle = function () {
    var text_name = this.getFieldValue('name');
    var dropdown_draw_triangle_type = this.getFieldValue('draw_triangle_type');
    var value_draw_triangle_x1 = Blockly.Arduino.valueToCode(this, 'draw_triangle_x1', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_triangle_y1 = Blockly.Arduino.valueToCode(this, 'draw_triangle_y1', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_triangle_x2 = Blockly.Arduino.valueToCode(this, 'draw_triangle_x2', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_triangle_y2 = Blockly.Arduino.valueToCode(this, 'draw_triangle_y2', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_triangle_x3 = Blockly.Arduino.valueToCode(this, 'draw_triangle_x3', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_triangle_y3 = Blockly.Arduino.valueToCode(this, 'draw_triangle_y3', Blockly.Arduino.ORDER_ATOMIC);
    var value_color = Blockly.Arduino.valueToCode(this, 'color', Blockly.Arduino.ORDER_ATOMIC) || '1';
    var code = text_name + '.' + dropdown_draw_triangle_type + 'Triangle(' + value_draw_triangle_x1 + ', ' + value_draw_triangle_y1 + ', ' + value_draw_triangle_x2 + ', ' + value_draw_triangle_y2 + ', ' + value_draw_triangle_x3 + ', ' + value_draw_triangle_y3 + ', ' + value_color + ');\n';
    return code;
};

Blockly.Arduino.epd_draw_bitmap = function () {
    var text_name = this.getFieldValue('name');
    var value_draw_bitmap_x = Blockly.Arduino.valueToCode(this, 'draw_bitmap_x', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_bitmap_y = Blockly.Arduino.valueToCode(this, 'draw_bitmap_y', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_bitmap_data = Blockly.Arduino.valueToCode(this, 'draw_bitmap_data', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_bitmap_width = Blockly.Arduino.valueToCode(this, 'draw_bitmap_width', Blockly.Arduino.ORDER_ATOMIC);
    var value_draw_bitmap_height = Blockly.Arduino.valueToCode(this, 'draw_bitmap_height', Blockly.Arduino.ORDER_ATOMIC);
    var value_color = Blockly.Arduino.valueToCode(this, 'color', Blockly.Arduino.ORDER_ATOMIC) || '1';
    var code = text_name + '.drawBitmap(' + value_draw_bitmap_data + ', ' + value_draw_bitmap_x + ', ' + value_draw_bitmap_y + ', ' + value_draw_bitmap_width + ', ' + value_draw_bitmap_height + ', ' + value_color + ');\n';
    return code;
};

})();