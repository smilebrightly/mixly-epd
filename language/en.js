Blockly.Msg.QMC5883L_INIT = 'Initialize QMC5883L electronic compass';
Blockly.Msg.QMC5883L_USE = 'use';
Blockly.Msg.QMC5883L_REFRESH = 'refresh data';
Blockly.Msg.QMC5883L_GET_AZIMUTH = 'get Azimuth';